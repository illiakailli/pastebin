# fosrig project notes 

- alternative names
	- rigos, fosrig

- project description
	- goal is to help user with building customized linux distributive based on already existing one
	- provides simple python api to accomplish the goal

- automation tools and libraries that may be useful
	- vagrant https://learn.hashicorp.com/collections/vagrant/getting-started
	- vagrant python bindings https://github.com/todddeluca/python-vagrant
	- ssh python bindings https://github.com/ParallelSSH/parallel-ssh
	- lazy object proxy https://github.com/ionelmc/python-lazy-object-proxy

- mvp scenarios
	- support linux host os
	- run linux on vm from image using simple api call
			- use kvm for virtualization
			- use alpine linux as base?
	- enumerate installed os apps, services and processes
	- augment basic apps, services and processes with detailed information 
	  pipabout their role in a system

- nice to have scenarios
	- support host oses: macos
	- query basic information about running oses
		- each running kvm instance with os should be represented as a separate object
		- use ssh to query needed information
	- create bootable installation image from currently running os
		- should preserve os state so that it can be restored from the image
	- shut down and reboot runnung oses
	- augment each services and processes with detailed information about its role in a system
	- show dependency graph for services and processes
	- enumerate enabled and running kernel modules
		- augment each kernel module with detailed information about its role in a system
	- disable/kill os services and processes
	- test basic os functions
		- ssh
		- internet availability
		- package installation from remote repository
		- package installation from local folder
	- determine minimal number of kernel modules, services and processes needed for os to function
		- disable as many services, startup processes, kernel modules as possible until
			- os no longer boots up
			- one of basic os functions stops working
	- install, enable and automatically configure firewall for maximum security (deny all by default)

- advanced scenarios
	- install and automatically configure application firewall
	- download kernel sources
	- recompile kernel from sources
	- inspect kernel sources to detect telemetry, if any
	- run dockerized distributives instead of kvm
	- implement cli wrapper around python api
